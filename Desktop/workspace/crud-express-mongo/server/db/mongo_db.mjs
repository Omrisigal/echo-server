
import user_model from "../modules/user/user.model.mjs";

export async function createUser(data) {
    const result = await user_model.create(data);
    return result;
}

export async function getAllUsers(selectors = null, skip = null, limit = null) {
    if(selectors){
        selectors = selectors.join(" ");
    }
    const users = await user_model.find()
                            .select(selectors)
                            .skip(skip).limit(limit);
    return users;
}

export async function getUserByID(id) {
    const user = await user_model.findById(id)
    return user;
}

export async function deleteUserByID( id){
    const user = await user_model.findByIdAndRemove(id);
    return user;
}


export async function updateUser(id, data){
    const user = await user_model.findByIdAndUpdate(id, data,  {new: true, upsert: false });
    return user;
}
