// require('dotenv').config();
import express from 'express'
import morgan from 'morgan'
import log from '@ajar/marker'
import cors from 'cors'

import {connect_db} from './db/mongoose.connection.mjs';
import user_router from './modules/user/user.router.mjs';

import {errorHandler,not_found} from './middleware/errors.handler.mjs';   


class App {

  constructor() {

    this.app = express();
    this.applyGlobalMiddleware()
    // routing
    this.app.use('/api/users', user_router);
    // central error handling
    this.app.use(errorHandler);    
    //when no routes were matched...
    this.app.use('*', not_found)

    this.startServer()

  }
  applyGlobalMiddleware(){
     // middleware
    this.app.use(cors());
    this.app.use(morgan('dev'))
  }
  async startServer() {
    const { PORT ,HOST, DB_URI } = process.env;
    await connect_db(DB_URI);  
    await this.app.listen(PORT,HOST);
    log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
  }
}

const myServerInstance = new App();


