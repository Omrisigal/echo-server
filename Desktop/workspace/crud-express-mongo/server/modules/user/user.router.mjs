/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
import {generalValidator} from "../../middleware/route.validator.mjs";
import user_model from "./user.model.mjs";
import {createUser,getAllUsers,getUserByID, updateUser, deleteUser, paginate} from "./user.controller.mjs";
import express from 'express';
import log from '@ajar/marker';

const router = express.Router();
const CREATE = "CREATE";
const UPDATE = "UPDATE";

// parse json req.body on post routes
router.use(express.json())

// CREATES A NEW USER
router.post("/", generalValidator(CREATE), raw(createUser));

// GET ALL USERS
router.get("/", raw(getAllUsers));

// GETS A SINGLE USER
router.get("/:id", raw(getUserByID));

// UPDATES A SINGLE USER
router.put("/:id", generalValidator(UPDATE),raw(updateUser));

// DELETES A USER
router.delete("/:id", raw(deleteUser));

// GET A BATCH OF USERS
router.get("/pagination/:page/:batch_size",raw(paginate));

export default router;
