import Joi from "joi";


function rulebuilder(){
    let first_name = Joi.string().alphanum().min(3).max(30);
    let last_name = Joi.string().alphanum().min(3).max(30);
    let email = Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } });
    return {first_name, last_name, email};
}

  export function generalValidator(schemaName) {
    return async function validator (req ,res, next) {
        const first_name = req.body.first_name;
        const last_name = req.body.last_name;
        const email = req.body.email;
        let rules =  rulebuilder();
        try{
            switch (schemaName){
                case 'CREATE':
                    for (const key in rules) {
                        rules[key] = rules[key].required();
                    }
                    const create_schema = Joi.object().keys(rules);
                    let x = await create_schema.validateAsync({first_name,last_name,email});
                    break;
                case 'UPDATE':
                    const update_schema = Joi.object().keys(rules);
                    await update_schema.validateAsync({first_name,last_name,email});
                    break;
            }
            next();
        }
        catch(err){
            next(err);
        }
      }
  }