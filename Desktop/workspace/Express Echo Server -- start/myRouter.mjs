
import express from 'express';

const router = express.Router();
let routeMiddle = (res,req,next)=>{
    console.log("route middleware function");
    next();
}
router.get('/users', routeMiddle, (req, res) => {
    res.status(200).send('Route Get all Users')
})

router.get('/', routeMiddle, (req, res) => {
    res.status(200).send('Routeeee')
})

export default router;