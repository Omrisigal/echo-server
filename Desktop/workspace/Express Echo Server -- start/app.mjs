import express from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import router from './myRouter.mjs';

const { PORT, HOST } = process.env;

const app = express()

app.use(morgan('dev') ); 
let appMiddle = (res,req,next)=>{
    console.log("appMiddle  middleware function");
    next();
}
app.use(appMiddle); 

let funcMiddle = (res,req,next)=>{
    console.log("normal users function middleware function");
    next();
}

app.use('/router',router);

app.get('/users', funcMiddle, (req, res) => {
    res.status(200).send('normal Get all Users')
})


app.listen(PORT, HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

app.use('*',  (req, res,next) => {
    const full_url = new URL(req.url, `http://${req.headers.host}`);
    res.status(404).json(` - 404 - url ${full_url.href+full_url.pathname} was not found BY OMRI`)
    // res.status(404).send(` - 404 - url ${req.url} was not found BY OMRI `)
})

