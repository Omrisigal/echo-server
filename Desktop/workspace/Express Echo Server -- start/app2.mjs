import express from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';

const { PORT, HOST } = process.env;

const app = express()
const route = express.Router();
let appMiddle = ()=>{
    console.log("appMiddle  middleware function");
}
let userLog = ()=>{
    console.log("users function middleware function");
}
let someFunc = ()=>{
    console.log("route middleware function");
}
const toUpper = (req,res,next)=>{
    
        req.query.food = req.query?.food?.toUpperCase();
        req.query.town = req.query?.town?.toUpperCase();
    

    next()
}
app.use(morgan('dev') ); 
 //app level middleware
app.use(appMiddle); 

// app.use(toUpper);
app.use(express.json());

//route middleware
route.use(someFunc);

//function middleware
app.get('/users', (req, res) => {
    res.status(200).send('Get all Users')
})


// '/search?food=burger&town=ashdod'

//query
app.get('/search',function(req,res){
    let food = req.query.food;
    let city = req.query.town;
    res.status(200).send(`you should Read get a ${food} in ${city}`);
})
//params
app.get('/:food/:city',function(req,res){
    let food = req.params.food;
    let city = req.params.city;
    res.status(200).send(`you should Eat a ${food} in ${city}`);
})

// //body
app.post('/search',function(req,res){
    res.status(200).json(`you should  get a ${req.body?.food} in ${req.body?.city}`);
})

app.get('/hola',(req,res)=>{

    // res.set('Content-Type','text/html');

    let some_data = 'Inspired!';
    
    const markup = `<h1>Hello Express</h1>
                    <p>This is an example demonstrating some basic html markup<br/>
                        being sent and rendered in the browser</p>
                    <p>Prepare to be:</p>
                    <ul>
                        <li>Surprised!</li>
                        <li>Amazed!</li>
                        <li>${some_data}</li>  
                    </ul>`
    res.status(200).set('Content-Type', 'text/html').send(markup)
    // res.status(200).send(markup)
})

app.get('/',  (req, res) => {
    res.status(200).send('Hello Express!')
})

app.listen(PORT, HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});


//------------------------------------------
//         Express Echo Server
//------------------------------------------
/* challenge instructions

     - install another middleware - morgan
        configuring app middleware like so:
        app.use( morgan('dev') );

    -  define more routing functions that use

        - req.query - access the querystring part of the request url
        - req.params - access dynamic parts of the url
        - req.body - access the request body of a POST request
        
        in each routing function you want to pass some values to the server from the client
        and echo those back in the server response

    - return api json response
    - return html markup response

    - return 404 status with a custom response to unsupported routes


*/

app.use('*',  (req, res,next) => {
    const full_url = new URL(req.url, `http://${req.headers.host}`);
    res.status(404).json(` - 404 - url ${full_url.href+full_url.pathname} was not found BY OMRI`)
    // res.status(404).send(` - 404 - url ${req.url} was not found BY OMRI `)
})